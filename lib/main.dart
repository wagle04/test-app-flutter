import 'package:flutter/material.dart';
import 'package:test_app/configs/theme.dart';
import 'package:test_app/modules/home/ui/page/homePage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: AppTheme.themeData,
      home: HomePage(),
    );
  }
}
