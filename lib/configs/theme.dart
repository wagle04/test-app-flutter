import 'package:flutter/material.dart';

class AppTheme {
  static const Color primaryColor = Colors.teal;
  static const Color secondaryColor = Colors.red;

  static ThemeData themeData = ThemeData(
    primaryColor: primaryColor,
    accentColor: secondaryColor,
  );
}
