import 'package:dio/dio.dart';
import 'package:test_app/configs/backEndUrl.dart';

class DioService {
  Dio getDioClient() {
    return Dio(BaseOptions(baseUrl: backEndbaseUrl));
  }

  Future<Response> handleGetRequest(
    String path, {
    Options options,
    CancelToken cancleToken,
  }) async {
    Dio dio = getDioClient();
    try {
      return await dio.get(
        path,
        options: options,
        cancelToken: cancleToken,
      );
    } on DioError catch (e) {
      return Response(
        data: {'status': e.error, 'message': e.message},
        statusCode: e.response?.statusCode,
        requestOptions: null,
      );
    }
  }

  Future<Response> handlePostRequest(
    String path, {
    dynamic data,
    Options options,
    CancelToken cancleToken,
  }) async {
    Dio dio = getDioClient();
    try {
      return await dio.post(
        path,
        data: data,
        options: options,
        cancelToken: cancleToken,
      );
    } on DioError catch (e) {
      return Response(
        data: {'status': e.error, 'message': e.message},
        statusCode: e.response?.statusCode,
        requestOptions: null,
      );
    }
  }
}
