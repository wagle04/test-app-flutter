import 'package:flutter/material.dart';
import 'package:test_app/modules/home/model/post.dart';
import 'package:test_app/modules/home/ui/widget/postWidget.dart';

class PostList extends StatelessWidget {
  final List<Post> posts;

  const PostList({
    Key key,
    this.posts,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Posts,",
            style: TextStyle(fontSize: 20),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView.separated(
              itemBuilder: (context, i) {
                return PostWidget(post: posts[i]);
              },
              separatorBuilder: (context, i) {
                return SizedBox(height: 10);
              },
              itemCount: posts.length,
            ),
          ),
        ),
      ],
    );
  }
}
