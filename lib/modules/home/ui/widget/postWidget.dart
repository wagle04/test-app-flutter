import 'package:flutter/material.dart';
import 'package:test_app/modules/home/model/post.dart';

class PostWidget extends StatelessWidget {
  final Post post;

  const PostWidget({Key key, this.post}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        padding: EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "userId: ${post.userId}",
              style: TextStyle(
                color: Theme.of(context).accentColor,
              ),
            ),
            SizedBox(height: 3),
            Text(
              "id: ${post.id}",
              style: TextStyle(
                fontStyle: FontStyle.italic,
              ),
            ),
            SizedBox(height: 3),
            Text(
              "title: ${post.title}",
              style: TextStyle(
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 3),
            Text("body: ${post.body}"),
          ],
        ),
      ),
    );
  }
}
