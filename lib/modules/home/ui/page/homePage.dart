import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/common/ui/widget/progressIndicator.dart';
import 'package:test_app/modules/firstScreen/ui/page/firstScreenPage.dart';
import 'package:test_app/modules/home/service/cubit/postcubit/post_cubit.dart';
import 'package:test_app/modules/home/ui/widget/errorText.dart';
import 'package:test_app/modules/home/ui/widget/postList.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => PostCubit(),
      child: HomePageBody(),
    );
  }
}

class HomePageBody extends StatefulWidget {
  @override
  _HomePageBodyState createState() => _HomePageBodyState();
}

class _HomePageBodyState extends State<HomePageBody> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<PostCubit>(context).getPosts();
  }

  @override
  Widget build(BuildContext context) {
    final PostCubit postCubit = BlocProvider.of<PostCubit>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Page"),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Center(
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return FirstScreenPage();
                      },
                    ),
                  );
                },
                child: Text("Todos"),
              ),
            ),
          ),
        ],
      ),
      body: BlocBuilder<PostCubit, PostState>(
        builder: (context, state) {
          if (state is PostLoading) {
            return CProgressIndicator();
          } else if (state is PostLoaded) {
            return PostList(posts: postCubit.posts);
          }

          return ErrorText();
        },
      ),
    );
  }
}
