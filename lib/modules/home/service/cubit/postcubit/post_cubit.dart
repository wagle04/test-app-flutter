import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:test_app/common/service/dio/dioService.dart';
import 'package:test_app/modules/home/model/post.dart';

part 'post_state.dart';

class PostCubit extends Cubit<PostState> {
  PostCubit() : super(PostInitial());

  List<Post> posts = [];

  getPosts() async {
    emit(PostLoading());

    Response response = await DioService().handleGetRequest("/posts");
    if (response.statusCode == 200) {
      var data = response.data;

      for (var post in data) {
        posts.add(Post.fromJson(post));
      }

      emit(PostLoaded());
    } else {
      emit(PostError());
    }
  }
}
