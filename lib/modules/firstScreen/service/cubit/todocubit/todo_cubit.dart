import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:test_app/common/service/dio/dioService.dart';
import 'package:test_app/modules/firstScreen/model/todo.dart';

part 'todo_state.dart';

class TodoCubit extends Cubit<TodoState> {
  TodoCubit() : super(TodoInitial());

  List<Todo> todos = [];

  getTodos() async {
    emit(TodoLoading());

    Response response = await DioService().handleGetRequest("/todos");
    if (response.statusCode == 200) {
      var data = response.data;
      for (var todo in data) {
        todos.add(Todo.fromJson(todo));
      }

      emit(TodoLoaded());
    } else {
      emit(TodoError());
    }
  }
}
