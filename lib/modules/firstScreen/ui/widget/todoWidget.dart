import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_app/modules/firstScreen/model/todo.dart';

class TodoWidget extends StatelessWidget {
  final Todo todo;

  const TodoWidget({Key key, this.todo}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        padding: EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "userId: ${todo.userId}",
              style: TextStyle(
                color: Theme.of(context).accentColor,
              ),
            ),
            SizedBox(height: 3),
            Text(
              "id: ${todo.id}",
              style: TextStyle(
                fontStyle: FontStyle.italic,
              ),
            ),
            SizedBox(height: 3),
            Text(
              "title: ${todo.title}",
              style: TextStyle(
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 3),
            Row(
              children: [
                Text("completed:"),
                SizedBox(width: 5),
                Icon(
                  todo.completed
                      ? CupertinoIcons.check_mark_circled
                      : CupertinoIcons.multiply_circle,
                  color: todo.completed
                      ? Theme.of(context).primaryColor
                      : Theme.of(context).accentColor,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
