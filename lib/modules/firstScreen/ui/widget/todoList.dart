import 'package:flutter/material.dart';
import 'package:test_app/modules/firstScreen/model/todo.dart';
import 'package:test_app/modules/firstScreen/ui/widget/todoWidget.dart';

class TodoList extends StatelessWidget {
  final List<Todo> todos;

  const TodoList({
    Key key,
    this.todos,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Todos,",
            style: TextStyle(fontSize: 20),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView.separated(
              itemBuilder: (context, i) {
                return TodoWidget(todo: todos[i]);
              },
              separatorBuilder: (context, i) {
                return SizedBox(height: 10);
              },
              itemCount: todos.length,
            ),
          ),
        ),
      ],
    );
  }
}
