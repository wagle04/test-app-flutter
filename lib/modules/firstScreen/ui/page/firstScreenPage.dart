import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/common/ui/widget/progressIndicator.dart';
import 'package:test_app/modules/firstScreen/service/cubit/todocubit/todo_cubit.dart';
import 'package:test_app/modules/firstScreen/ui/widget/errorText.dart';
import 'package:test_app/modules/firstScreen/ui/widget/todoList.dart';

class FirstScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TodoCubit(),
      child: FirstScreenBody(),
    );
  }
}

class FirstScreenBody extends StatefulWidget {
  @override
  _FirstScreenBodyState createState() => _FirstScreenBodyState();
}

class _FirstScreenBodyState extends State<FirstScreenBody> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<TodoCubit>(context).getTodos();
  }

  @override
  Widget build(BuildContext context) {
    final TodoCubit todoCubit = BlocProvider.of<TodoCubit>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("First Screen Page"),
      ),
      body: BlocBuilder<TodoCubit, TodoState>(
        builder: (context, state) {
          if (state is TodoLoading) {
            return CProgressIndicator();
          } else if (state is TodoLoaded) {
            return TodoList(todos: todoCubit.todos);
          }

          return ErrorText();
        },
      ),
    );
  }
}
